package scrum.service.impl;

import javax.persistence.EntityManager;

import scrum.entity.impl.TaskImpl;
import scrum.service.TaskService;
import scrum.utile.EMFactory;

public class TaskServiceImpl implements TaskService {
		
	private EntityManager em;
	
	public TaskServiceImpl(){
		this.em = new EMFactory().getManager();
	}
	
	@Override
	public void create(TaskImpl task) {
		try {
			em.getTransaction().begin();
			em.persist(task);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		}
	}
	
	@Override
	public TaskImpl find(long id) {
		return em.find(TaskImpl.class, id);
	}

	
	@Override
	public void remove(long id) {
		try{
			em.getTransaction().begin();
			em.remove(em.find(TaskImpl.class, id));
			em.getTransaction().commit();
		} catch(Exception e){
			em.getTransaction().rollback();
			throw e;
		}
	}

}
