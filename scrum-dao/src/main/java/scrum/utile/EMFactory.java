package scrum.utile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMFactory {
	
	private static final String PERSIST_UNIT = "persistUnit";
	private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSIST_UNIT);
	
	public EntityManager getManager(){
		return factory.createEntityManager();
	}
}
