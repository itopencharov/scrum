package scrum.service.impl;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import scrum.entity.impl.TaskImpl;
import scrum.entity.impl.UserImpl;
import scrum.service.UserService;
import scrum.utile.EMFactory;

public class UserServiceImpl implements UserService {
	
	private EntityManager em;
	
	public UserServiceImpl() {
		em = new EMFactory().getManager();
	}
	
	@Override
	public void create(UserImpl user) {
		persistUser(user);
	}
	
	@Override
	public UserImpl find(long id) {
		return em.find(UserImpl.class, id);
	}
	
	@Override
	public void remove(long id) {
		try {
			em.getTransaction().begin();
			em.remove(find(id));
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		}
	}

	@Override
	public void assignTask(long userId, TaskImpl task) {
		UserImpl user = find(userId);
		user.getTasks().add(task);
		persistUser(user);
	}

	@Override
	public void removeTask(long userId) {
		UserImpl user = find(userId);
		user.setTasks(new ArrayList<TaskImpl>(5));
		persistUser(user);
	}
	
	private void persistUser(final UserImpl user){
		try {
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		}
	}
}
