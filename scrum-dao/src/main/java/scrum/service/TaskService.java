package scrum.service;

import scrum.entity.impl.TaskImpl;

public interface TaskService {
	
	void create(final TaskImpl task);
	
	TaskImpl find(final long id);
	
	void remove(final long id);
	
}
