<!doctype html>
<html ng-app>
	<head>
		<title>Users</title>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
		<script src="users.js"></script>
	</head>
	<body>
		<div ng-controller="Users">
			<ul class="users-containter">
				<li ng-repeat="user in users">
					{{user.name}} {{user.gender}}
				</li>
			</ul>
		</div>
	</body>
</html>