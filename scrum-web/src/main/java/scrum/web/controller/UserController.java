package scrum.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import scrum.service.UserService;
import scrum.service.impl.UserServiceImpl;
import scrum.web.model.User;

@Controller
@RequestMapping(value="/user")
public class UserController {
	
	@RequestMapping(method=RequestMethod.GET)
	public String displayUser(Model model){
		UserService service = new UserServiceImpl();
		User user = new User();
		user.setName(service.find(3).getName());
		model.addAttribute("user", user);
		
		return "user";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User user){
		System.out.println(user.getName());
		System.out.println(user.getGender());
		
		return "redirect:index.jsp";
	}
}
