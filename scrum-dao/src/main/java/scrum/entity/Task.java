package scrum.entity;

import java.sql.Date;

import scrum.entity.impl.UserImpl;

public interface Task {
	
	public long getId();
	
	public String getName();
	
	public Date getDeadline();
	
	public String getDescription();
	
	public UserImpl getUser();
	
	public void setId(final long id);

	public void setName(final String name);

	public void setDeadline(final Date deadline);

	public void setDescription(String description);

	public void setUser(UserImpl user);
}
