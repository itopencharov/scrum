package scrum.entity.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import scrum.entity.User;

@Entity
@Table(name = "user")
public class UserImpl implements User{

	private long id;
	private String name;
	private List<TaskImpl> tasks;
	
	public UserImpl(){}
	
	public UserImpl(final String name){
		this.tasks = new ArrayList<TaskImpl>(5);
		this.name = name;
	}
	
	@Id
	@GeneratedValue
	@Column(name="user_id")
	public long getId() {
		return id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	@OneToMany
	@JoinColumn(name="task_id")
	public List<TaskImpl> getTasks() {
		return tasks;
	}
	
	public void setId(final long id) {
		this.id = id;
	}
	
	public void setName(final String name) {
		this.name = name;
	}
	
	public void setTasks(final List<TaskImpl> tasks) {
		this.tasks = tasks;
	}
	
}
