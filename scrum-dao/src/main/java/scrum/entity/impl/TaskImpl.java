package scrum.entity.impl;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import scrum.entity.Task;

@Entity
@Table(name = "task")
public class TaskImpl implements Task{
	
	private long id;
	private String name;
	private String description;
	private Date deadline;
	private UserImpl user;
	
	TaskImpl(){}
	
	public TaskImpl(final String name, final Date deadline, final String description){
		this.name = name;
		this.deadline = deadline;
		this.description = description;
	}
	
	@Id
	@GeneratedValue
	@Column(name = "task_id")
	public long getId() {
		return id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	@Column(name = "deadline")
	public Date getDeadline() {
		return deadline;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	public UserImpl getUser() {
		return user;
	}
	
	public void setId(final long id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setDeadline(final Date deadline) {
		this.deadline = deadline;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUser(UserImpl user) {
		this.user = user;
	}
	
}
