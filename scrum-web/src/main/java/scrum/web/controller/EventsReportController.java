package scrum.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import scrum.web.model.User;

@RestController
public class EventsReportController {

	@RequestMapping("/users")
	public List<User> getUser() {
		List<User> users = new ArrayList<>();
		
		User u1 = new User();
		u1.setName("Ivo");
		u1.setGender("Male");
		User u2 = new User();
		u1.setName("Gosho");
		User u3 = new User();
		u1.setName("Kiro");
		users.add(u1);
		users.add(u2);
		users.add(u3);
		
		return users;
	}
}
