package scrum.service;

import scrum.entity.impl.TaskImpl;
import scrum.entity.impl.UserImpl;

public interface UserService {
	
	void create(final UserImpl user);
	
	UserImpl find(final long id);
	
	void remove(final long id);
	
	void assignTask(final long userId, final TaskImpl task);
		
	void removeTask(final long userId);
}
