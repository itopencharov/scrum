package scrum.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import scrum.service.TaskService;
import scrum.service.impl.TaskServiceImpl;
import scrum.web.model.Task;
import scrum.web.model.User;

@Controller
@RequestMapping(value="/task")
public class TaskController {
	
	@RequestMapping(method=RequestMethod.GET)
	public String task(Model model){
		Task task = new Task();
		model.addAttribute("task", task);
	
		return "task";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String addTask(@ModelAttribute("task") Task task){
		System.out.println(task.getName());
		
		return "redirect:index.jsp";
	}
}
