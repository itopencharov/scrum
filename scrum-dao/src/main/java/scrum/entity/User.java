package scrum.entity;

import java.util.List;

import scrum.entity.impl.TaskImpl;

public interface User {
	
	public long getId();
	
	public String getName();
	
	public List<TaskImpl> getTasks();
	
	public void setId(final long id);
	
	public void setName(final String name);
	
	public void setTasks(final List<TaskImpl> tasks);
}
